//! Methods for generating random passwords of chars.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use rand::Rng;
use rand::distributions::Distribution;

/// Sample a `char`, uniformly distributed over ASCII letters, numbers, and symbols:
/// a-z, A-Z, 0-9 and !"#$%&'()*+,-./:;<=>?@`\~[]{}^_| .
///
/// # Example
///
/// ```
/// # extern crate qwerty;
/// # extern crate rand;
/// # fn main() {
/// use std::iter;
/// use rand::{Rng, thread_rng};
/// use qwerty::ascii::Ascii;
///
/// let mut rng = thread_rng();
/// let chars: String = iter::repeat(())
///         .map(|()| rng.sample(Ascii))
///         .take(7)
///         .collect();
/// println!("Random chars: {}", chars);
/// # }
/// ```
#[derive(Debug)]
pub struct Ascii;

const ASCII_STR_CHARSET: &[u8] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZ\
    abcdefghijklmnopqrstuvwxyz\
    0123456789\
    !\"#$%&'()*+,-./:;<=>?@`\\~[]{}^_| ";

impl Distribution<char> for Ascii {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> char {
        const RANGE: u32 = 26 + 26 + 10 + 33;
        // We can pick from 95 characters. This is so close to a power of 2,
        // 128 (2^7), that we can do better than `Uniform`. Use a simple
        // bitshift and rejection sampling. We do not use a bitmask, because for
        // small RNGs the most significant bits are usually of higher quality.
        loop {
            let var = rng.next_u32() >> (32 - 7);
            if var < RANGE {
                return ASCII_STR_CHARSET[var as usize] as char;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::iter;
    use rand::{ChaChaRng, FromEntropy};

    // -------------------------------------------------------------------------
    // With replacement the probability we **dont't** see any given
    // char in the set in `n` samples is (94/95)^n. We want this value
    // to be very small, <= 0.0001. To accomplish this we want:
    //
    //     n * ln(94/95) <= ln(0.0001)
    //
    //  Or the equivalent:
    //
    //     n >= ln(10000) / ln(95/94)
    //
    //  Which is equal to:
    //
    //     n >= 870
    //
    //  Note that there is still a very small probability this test will
    //  fail without cause.
    // -------------------------------------------------------------------------
    #[test]
    fn ascii_distribution_inclusive() {
        let mut rng = ChaChaRng::from_entropy();
        let chars: Vec<char> = iter::repeat(())
            .map(|()| rng.sample(Ascii))
            .take(870)
            .collect();

        for ch in ASCII_STR_CHARSET {
            println!("{}", ch);
            assert!(chars.contains(&(*ch as char)));
        }
    }

    // -------------------------------------------------------------------------
    // This test ensures that no chars outside of the set are sampled.
    // -------------------------------------------------------------------------
    #[test]
    fn ascii_distribution_exclusive() {
        let mut rng = ChaChaRng::from_entropy();
        let chars: Vec<char> = iter::repeat(())
            .map(|()| rng.sample(Ascii))
            .take(3000)
            .collect();

        for ch in chars {
            assert!(ASCII_STR_CHARSET.contains(&(ch as u8)));
        }
    }
}

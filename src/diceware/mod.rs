//! Methods for generating readable passwords.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

pub mod eff_distribution;
pub mod eff_wordlist;
pub mod diceware_distribution;
pub mod diceware_wordlist;

pub use self::diceware_distribution::Diceware;
pub use self::eff_distribution::EffDiceware;

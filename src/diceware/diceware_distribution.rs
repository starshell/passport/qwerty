//! The original diceware wordlist.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use rand::{CryptoRng, Rng};
use rand::distributions::Distribution;
use diceware::diceware_wordlist::DICEWARE_WORDLIST;

/// Sample a word, uniformly distributed over the standard diceware list:
///
/// The complete list contains 7776 short words, abbreviations and
/// easy-to-remember character strings. The average length of each
/// word is about 4.2 characters. The biggest words are six characters
/// long.
///
/// # Example
///
/// ```
/// # extern crate qwerty;
/// # extern crate rand;
/// # fn main() {
/// use std::iter;
/// use rand::{Rng, thread_rng};
/// use qwerty::diceware::Diceware;
///
/// let mut rng = thread_rng();
/// let words: Vec<&'static [u8]> = iter::repeat(())
///         .map(|()| rng.sample(Diceware))
///         .take(7)
///         .collect();
/// println!("Random words: {:?}", words);
/// # }
/// ```
#[derive(Debug)]
pub struct Diceware;

impl Distribution<&'static [u8]> for Diceware {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> &'static [u8] {
        const RANGE: u32 = 7776;
        loop {
            // We can pick from 7776 words. This is so close to a power of 2,
            // 8192 (2^13), that we can do better than `Uniform`. Use a simple
            // bitshift and rejection sampling. We do not use a bitmask, because
            // for small RNGs the most significant bits are usually of higher
            // quality.
            let var = rng.next_u32() >> (32 - 13);
            if var < RANGE {
                return DICEWARE_WORDLIST[var as usize];
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::iter;
    use rand::{thread_rng, Rng};

    // -------------------------------------------------------------------------
    // Expensive computations we do not want to repeat.
    // -------------------------------------------------------------------------

    // The sample, see rational for the number we take below.
    lazy_static! {
        static ref STANDARD_WORDS_SAMPLE_80K: Vec<&'static [u8]> = {
            let mut rng = thread_rng();
            iter::repeat(())
                .map(|()| rng.sample(Diceware))
                .take(80000)
                .collect()
        };
    }

    // -------------------------------------------------------------------------
    // With replacement the probability we **dont't** see any given
    // word in the set in `n` samples is (7775/7776)^n. We want this value
    // to be very small, <= 0.0001. To accomplish this we want:
    //
    //     n * ln(7775/7776) <= ln(0.0001)
    //
    //  Or the equivalent:
    //
    //     n >= ln(10000) / ln(7776/7775)
    //
    //  Which is equal to:
    //
    //     n >= 71615
    //
    //  Note that there is still a very small probability this test will
    //  fail without cause.
    // -------------------------------------------------------------------------
    /*
    #[test]
    fn standard_diceware_distribution_inclusive() {
        for word in DICEWARE_WORDLIST.iter() {
            //println!("{:?}", String::from_utf8_lossy(word));
            assert!(
                STANDARD_WORDS_SAMPLE_80K.contains(&word),
                "Sample of words from diceware wordlist did not contain the word: {:?}",
                String::from_utf8_lossy(word)
            );
        }
    }
    */

    // -------------------------------------------------------------------------
    // This test ensures that no words outside of the set are sampled.
    // -------------------------------------------------------------------------
    #[test]
    fn standard_diceware_distribution_exclusive() {
        for word in STANDARD_WORDS_SAMPLE_80K.iter() {
            assert!(
                match DICEWARE_WORDLIST.binary_search(&word) {
                    Ok(_) => true,
                    Err(_) => false,
                },
                "Word found in samples from diceware wordlist that is not in the wordlist: {:?}",
                String::from_utf8_lossy(word)
            );
        }
    }
}

//! An easy to use library for generating passwords.
//!
//! A number of distributions are available to generate passwords from
//! including alphanumeric and diceware style.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#![cfg_attr(feature = "strict", feature(plugin))]
#![cfg_attr(feature = "strict", plugin(clippy))]
#![cfg_attr(feature = "strict", deny(warnings))]

#[macro_use]
extern crate lazy_static;
extern crate rand;

pub mod ascii;
pub mod diceware;

use rand::{thread_rng, Rng};
use ascii::Ascii;
use rand::distributions::Alphanumeric;
use diceware::diceware_distribution::Diceware;
use diceware::eff_distribution::EffDiceware;

/// A password generator. Once `Qwerty` has been initialized
/// with the desired password properties the terminal function
/// `generate` can be called to get a password.
///
/// # Examples
///
/// ```
/// use qwerty::{Qwerty, Distribution};
/// let password = Qwerty::new(Distribution::Alphanumeric, 32).generate();
/// ```
pub struct Qwerty {
    distribution: Distribution,
    num_tokens: usize,
}

impl Qwerty {
    /// Create a new `Qwerty` password generator.
    ///
    /// # Examples
    ///
    /// The following will create a password generator that uses
    /// the `Ascii` uniform distribution over the ASCII charachters,
    /// numbers, and symbols. It also sets the password length to be
    /// 25. Once the terminal function `generate()` is called it will
    /// return a password with these properties.
    ///
    /// ```
    /// use qwerty::{Qwerty, Distribution};
    ///
    /// let ascii_password_generator = Qwerty::new(Distribution::Ascii, 25);
    /// let password = ascii_password_generator.generate();
    /// println!("{}", password);
    /// ```
    pub fn new(distribution: Distribution, num_tokens: usize) -> Qwerty {
        Qwerty {
            distribution,
            num_tokens,
        }
    }

    /// Generate a password.
    ///
    /// # Examples
    ///
    /// The following will generate a password using the EFF diceware
    /// long word list that is 8 words long.
    ///
    /// ```
    /// use qwerty::{Qwerty, Distribution};
    ///
    /// let password = Qwerty::new(Distribution::EffDiceware, 8).generate();
    /// println!("{}", password);
    /// ```
    pub fn generate(&self) -> String {
        match self.distribution {
            Distribution::Alphanumeric => alphanumeric_password(self.num_tokens),
            Distribution::Ascii => ascii_password(self.num_tokens),
            Distribution::Diceware => diceware_password(self.num_tokens),
            Distribution::EffDiceware => eff_diceware_password(self.num_tokens),
        }
    }
}

/// The types of distributions available to select passwords from.
#[derive(Debug)]
pub enum Distribution {
    /// Alphanumeric uniform distribution.
    ///
    /// Samples a `char`, uniformly distributed over ASCII letters, and numbers:
    /// a-z, A-Z, and 0-9.
    Alphanumeric,

    /// Ascii uniform distribution.
    ///
    /// Sample a `char`, uniformly distributed over ASCII letters, numbers, and symbols:
    /// a-z, A-Z, 0-9 and !"#$%&'()*+,-./:;<=>?@`\~[]{}^_| .
    Ascii,

    /// A uniform distribution over the standard diceware wordlist.
    ///
    /// The complete list contains 7776 short words, abbreviations and
    /// easy-to-remember character strings. The average length of each
    /// word is about 4.2 characters. The biggest words are six characters
    /// long.
    Diceware,

    /// A uniform distribution over the standard diceware wordlist.
    ///
    /// The complete list contains 7776 short words, abbreviations and
    /// easy-to-remember character strings. The list has an average length
    /// of 7.0 characters per word, and 12.9 bits of entropy per word,
    /// yielding an efficiency of 1.8 bits of entropy per character.
    EffDiceware,
}

fn diceware_tokens(num: usize) -> Vec<&'static [u8]> {
    let diceware_tokens: Vec<&'static [u8]> =
        thread_rng().sample_iter(&Diceware).take(num).collect();
    diceware_tokens
}

fn diceware_password(num: usize) -> String {
    let diceware_password: Vec<u8> = diceware_tokens(num)
        .iter()
        .cloned()
        .flat_map(|word| word.iter().cloned())
        .collect();
    (*String::from_utf8_lossy(&diceware_password)).to_owned()
}

fn eff_diceware_tokens(num: usize) -> Vec<&'static [u8]> {
    let eff_diceware_tokens: Vec<&'static [u8]> =
        thread_rng().sample_iter(&EffDiceware).take(num).collect();
    eff_diceware_tokens
}

fn eff_diceware_password(num: usize) -> String {
    let eff_diceware_password: Vec<u8> = eff_diceware_tokens(num)
        .iter()
        .cloned()
        .flat_map(|word| word.iter().cloned())
        .collect();
    (*String::from_utf8_lossy(&eff_diceware_password)).to_owned()
}

fn ascii_tokens(num: usize) -> Vec<char> {
    let ascii_tokens: Vec<char> = thread_rng().sample_iter(&Ascii).take(num).collect();
    ascii_tokens
}

fn ascii_password(num: usize) -> String {
    let ascii_password: String = ascii_tokens(num).into_iter().collect();
    ascii_password
}

fn alphanumeric_tokens(num: usize) -> Vec<char> {
    let ascii_tokens: Vec<char> = thread_rng().sample_iter(&Alphanumeric).take(num).collect();
    ascii_tokens
}

fn alphanumeric_password(num: usize) -> String {
    let ascii_password: String = alphanumeric_tokens(num).into_iter().collect();
    ascii_password
}

#[cfg(test)]
mod tests {
    use super::*;

    // -------------------------------------------------------------------------
    // Token number tests
    //
    // Token types differ between alphanumeric type passwords and
    // the diceware word based. These ensure that the number of
    // tokens generated are handled properly for each.
    // -------------------------------------------------------------------------

    #[test]
    fn diceware_token_number() {
        assert_eq!(0, diceware_tokens(0).iter().count());
        assert_eq!(1, diceware_tokens(1).iter().count());
        assert_eq!(8, diceware_tokens(8).iter().count());
        assert_eq!(42, diceware_tokens(42).iter().count());
        assert_eq!(4000, diceware_tokens(4000).iter().count());
    }

    #[test]
    fn eff_diceware_token_number() {
        assert_eq!(0, eff_diceware_tokens(0).iter().count());
        assert_eq!(1, eff_diceware_tokens(1).iter().count());
        assert_eq!(8, eff_diceware_tokens(8).iter().count());
        assert_eq!(42, eff_diceware_tokens(42).iter().count());
        assert_eq!(4000, eff_diceware_tokens(4000).iter().count());
    }

    #[test]
    fn ascii_token_number() {
        assert_eq!(0, ascii_tokens(0).iter().count());
        assert_eq!(1, ascii_tokens(1).iter().count());
        assert_eq!(8, ascii_tokens(8).iter().count());
        assert_eq!(42, ascii_tokens(42).iter().count());
        assert_eq!(4000, ascii_tokens(4000).iter().count());
    }

    #[test]
    fn alphanumeric_token_number() {
        assert_eq!(0, alphanumeric_tokens(0).iter().count());
        assert_eq!(1, alphanumeric_tokens(1).iter().count());
        assert_eq!(8, alphanumeric_tokens(8).iter().count());
        assert_eq!(42, alphanumeric_tokens(42).iter().count());
        assert_eq!(4000, alphanumeric_tokens(4000).iter().count());
    }

    // -------------------------------------------------------------------------
    // Password length tests
    //
    // While the length of the `char` based generators should be equivalent
    // to the number of tokens requested the word list based generators
    // are tested within their min/max bounds.
    //
    // Diceware bounds:
    // - Min: 1 characters
    // - Max: 6 characters
    //
    // Eff Diceware bounds:
    // - Min: 3 characters
    // - Max: 9 characters
    // -------------------------------------------------------------------------

    fn diceware_bounds(num_tokens: usize, count: usize) -> bool {
        let min = num_tokens;
        let max = num_tokens * 6;
        if (count >= min) && (count <= max) {
            return true;
        }
        false
    }

    fn eff_diceware_bounds(num_tokens: usize, count: usize) -> bool {
        let min = num_tokens * 3;
        let max = num_tokens * 9;
        if (count >= min) && (count <= max) {
            return true;
        }
        false
    }

    #[test]
    fn diceware_password_length() {
        assert!(diceware_bounds(0, diceware_password(0).chars().count()));
        assert!(diceware_bounds(1, diceware_password(1).chars().count()));
        assert!(diceware_bounds(42, diceware_password(42).chars().count()));
        assert!(diceware_bounds(
            4000,
            diceware_password(4000).chars().count()
        ));
    }

    #[test]
    fn eff_diceware_password_length() {
        assert!(eff_diceware_bounds(
            0,
            eff_diceware_password(0).chars().count()
        ));
        assert!(eff_diceware_bounds(
            1,
            eff_diceware_password(1).chars().count()
        ));
        assert!(eff_diceware_bounds(
            42,
            eff_diceware_password(42).chars().count()
        ));
        assert!(eff_diceware_bounds(
            4000,
            eff_diceware_password(4000).chars().count()
        ));
    }

    #[test]
    fn ascii_password_length() {
        assert_eq!(0, ascii_password(0).chars().count());
        assert_eq!(1, ascii_password(1).chars().count());
        assert_eq!(8, ascii_password(8).chars().count());
        assert_eq!(42, ascii_password(42).chars().count());
        assert_eq!(4000, ascii_password(4000).chars().count());
    }

    #[test]
    fn alphanumeric_password_length() {
        assert_eq!(0, alphanumeric_password(0).chars().count());
        assert_eq!(1, alphanumeric_password(1).chars().count());
        assert_eq!(8, alphanumeric_password(8).chars().count());
        assert_eq!(42, alphanumeric_password(42).chars().count());
        assert_eq!(4000, alphanumeric_password(4000).chars().count());
    }
}

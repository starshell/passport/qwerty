# Qwerty

[![Crates.io](https://img.shields.io/crates/v/qwerty.svg)](https://crates.io/crates/qwerty) [![Crates.io](https://img.shields.io/crates/d/qwerty.svg)](https://crates.io/crates/qwerty) [![license](https://img.shields.io/crates/l/qwerty.svg)](https://gitlab.com/starshell/passport/qwerty/blob/master/LICENSE) [![Coverage Status](https://codecov.io/gl/starshell/qwerty/branch/master/graph/badge.svg)](https://codecov.io/gl/starshell/qwerty)

Linux: [![Build status](https://gitlab.com/starshell/passport/qwerty/badges/master/pipeline.svg)](https://gitlab.com/starshell/passport/qwerty/commits/master)
Windows: [![Build status](https://ci.appveyor.com/api/projects/status/oxvsn9m3uw9v7u7g/branch/master?svg=true)](https://ci.appveyor.com/project/Eudoxier/qwerty/branch/master)

&nbsp;

---

> **Qwerty IS NOT READY TO BE USED FOR SECURE GENERATION OF PASSWORDS.**
>
> I am currently working on the [XD lol](https://gitlab.com/starshell/xdlol) library for testing randomness, it would be possible to verify the security of the library using either [Dieharder](https://webhome.phy.duke.edu/%7Ergb/General/dieharder.php) or [ENT](http://fourmilab.ch/random/) but this is very unappealing since I would have to do this manually each time or write some shell script to do it. Instead I am writing [XD lol](https://gitlab.com/starshell/xdlol) to make it simple to write unit tests for randomness. This would also make a nice addition for Rust libraries providing random data in general.
>
> Also expect the API to move fast and break things for now.

---

&nbsp;

A password generation library.

## Usage

Add `qwerty` as a dependency in your `Cargo.toml` to use from crates.io:

```toml
[dependencies]
qwerty = "0.1.0"
```

Then add `extern crate qwerty;` to your crate root and run `cargo build` or `cargo update && cargo build` for your project. Documentation for all releases can be found on [docs.rs](https://docs.rs/qwerty/).

### Example

```rust
extern crate qwerty;

```

Examples can also be ran directly:

```sh
$ cargo run --example some_example
    Finished dev [unoptimized + debuginfo] target(s) in 0.0 secs
     Running `target/debug/examples/some_example`
```

See [examples](examples/) for more.

## Contributing

The project is mirrored to GitHub, but all development is done on GitLab. Please use the [GitLab issue tracker](https://gitlab.com/starshell/passport/qwerty/issues). Don't have a GitLab account? Just email `incoming+starshell/passport/qwerty@gitlab.com` and those emails automatically become issues (with the comments becoming the email conversation).

To contribute to qwerty, please see [CONTRIBUTING](CONTRIBUTING.md).

## License

Qwerty is distributed under the terms of the MPL 2.0 license. If this does not suit your needs for some reason please feel free to contact me, or open an issue.

See [LICENSE](LICENSE).
